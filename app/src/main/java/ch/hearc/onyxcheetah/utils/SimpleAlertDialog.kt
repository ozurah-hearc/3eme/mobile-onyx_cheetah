package ch.hearc.onyxcheetah.utils

import android.app.AlertDialog
import android.content.Context
import ch.hearc.onyxcheetah.R

class SimpleAlertDialog {
    companion object {
        fun show(
            context: Context,
            title: String = context.getString(R.string.popup_confirm),
            message: String = context.getString(R.string.popup_confirm_message),
            positiveButton: String = context.getString(R.string.popup_button_confirm),
            negativeButton: String = context.getString(R.string.popup_button_cancel),
            onOk: () -> Unit = {},
            onCancel: () -> Unit = {}
        ) {
            val builder = AlertDialog.Builder(context)
            builder.setTitle(title)
            builder.setMessage(message)
            builder.setPositiveButton(positiveButton) { _, _ -> onOk() }
            builder.setNegativeButton(negativeButton) { _, _ -> onCancel() }
            builder.show()
        }
    }
}