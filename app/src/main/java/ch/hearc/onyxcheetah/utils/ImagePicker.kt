package ch.hearc.onyxcheetah.utils

import android.content.DialogInterface
import android.net.Uri
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import ch.hearc.onyxcheetah.BuildConfig
import ch.hearc.onyxcheetah.R
import java.io.File
import java.io.FileOutputStream

class ImagePicker {
    private val fragment: Fragment
    private val galleryResult: ActivityResultLauncher<String>?
    private val cameraResult: ActivityResultLauncher<Uri>?
    private var imageUri: Uri? = null

    // Constructor
    constructor(
        fragment: Fragment,
        onImagePicked: (Uri) -> Unit
    ) {
        this.fragment = fragment

        // Handle image picked from gallery
        this.galleryResult = fragment.registerForActivityResult(ActivityResultContracts.GetContent()) { uri ->
            if (uri != null) {
                imageUri = createPersistentFile(uri)
                onImagePicked(imageUri!!)
            }
        }

        // Handle image taken with camera
        this.cameraResult = fragment.registerForActivityResult(ActivityResultContracts.TakePicture()) { isSuccess ->
            if (isSuccess) {
                if (imageUri != null) {
                    imageUri = createPersistentFile(imageUri!!)
                    onImagePicked(imageUri!!)
                }
            }
        }
    }

    private fun createPersistentFile(uri: Uri): Uri? {
        // Create a file in a persistent directory in the app's internal storage,
        // so that the image is accessible after relaunching the app
        val appContext = fragment.requireContext().applicationContext
        val persistentDir = appContext.filesDir
        val filename = generateRandomFilename("png")
        val persistentFile = File(persistentDir, filename)
        // Copy the image from the given URI to the persistent file
        val inputStream = appContext.contentResolver.openInputStream(uri)
        val outputStream = FileOutputStream(persistentFile)
        inputStream?.copyTo(outputStream)
        inputStream?.close()
        outputStream.close()
        // Return the URI of the persistent file
        return Uri.fromFile(persistentFile)
    }

    private fun generateRandomFilename(extension: String): String {
        // Create a 16-character random string to use as filename
        val allowedChars = ('a'..'z') + ('A'..'Z') + ('0'..'9')
        return (1..16)
            .map { allowedChars.random() }
            .joinToString("") + ".$extension"
    }

    private fun pickImageFromGallery() {
        galleryResult?.launch("image/*")
    }

    private fun takeImageFromCamera() {
        fragment.lifecycleScope.launchWhenStarted {
            imageUri = getTmpFileUri()
            cameraResult?.launch(imageUri)
        }
    }

    private fun getTmpFileUri(): Uri {
        val cacheDir = fragment.requireContext().cacheDir
        val tmpFile = File.createTempFile("tmp", ".png", cacheDir).apply {
            createNewFile()
            deleteOnExit()
        }

        val appContext = fragment.requireContext().applicationContext
        return FileProvider.getUriForFile(
            appContext,
            "${BuildConfig.APPLICATION_ID}.provider",
            tmpFile
        )
    }

    public fun askUserForImage() {
        val choices = arrayOf(
            "🖼 " + fragment.getString(R.string.image_picker_gallery),
            "📷 " + fragment.getString(R.string.image_picker_camera)
        )
        val builder = fragment.activity?.let { AlertDialog.Builder(it) } ?: return
        builder.setTitle(R.string.image_picker_title)
        builder.setItems(choices) { _: DialogInterface, which: Int ->
            when (which) {
                0 -> pickImageFromGallery()
                1 -> takeImageFromCamera()
            }
        }
        builder.show()
    }
}
