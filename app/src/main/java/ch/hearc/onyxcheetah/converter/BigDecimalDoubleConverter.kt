package ch.hearc.onyxcheetah.converter

import androidx.room.TypeConverter
import java.math.BigDecimal


// Source : https://stackoverflow.com/questions/47651743/how-can-i-store-currency-in-room-db
class BigDecimalDoubleConverter {
    @TypeConverter
    fun bigDecimalToDouble(input: BigDecimal?): Double {
        return input?.toDouble() ?: 0.0
    }

    @TypeConverter
    fun doubleToBigDecimal(input: Double?): BigDecimal {
        if (input == null) return BigDecimal.ZERO
        return BigDecimal.valueOf(input) ?: BigDecimal.ZERO
    }
}