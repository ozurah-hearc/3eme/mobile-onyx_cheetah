package ch.hearc.onyxcheetah.enums

import ch.hearc.onyxcheetah.R

enum class FuelType(val value: Int) {
    SUPER95(R.string.enum_FUELTYPE_SUPER95),
    SUPER98(R.string.enum_FUELTYPE_SUPER98),
    DIESEL(R.string.enum_FUELTYPE_DIESEL)
}