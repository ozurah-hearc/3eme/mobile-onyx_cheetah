package ch.hearc.onyxcheetah.enums

enum class AutoFuelCalc {
    LITERS,
    PRICE_PER_LITER,
    TOTAL_PRICE
}