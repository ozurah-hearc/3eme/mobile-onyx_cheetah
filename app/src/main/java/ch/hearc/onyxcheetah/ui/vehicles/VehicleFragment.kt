package ch.hearc.onyxcheetah.ui.vehicles

import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TableRow
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import ch.hearc.onyxcheetah.R
import ch.hearc.onyxcheetah.data.Refuel
import ch.hearc.onyxcheetah.data.Vehicle
import ch.hearc.onyxcheetah.databinding.FragmentVehicleBinding
import ch.hearc.onyxcheetah.ui.parts.VehiclePartFragment
import ch.hearc.onyxcheetah.viewModel.VehiclesViewModel
import java.text.DateFormat
import java.text.DecimalFormat
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*

class VehicleFragment : Fragment() {
    private lateinit var binding: FragmentVehicleBinding

    private lateinit var vehicleSource: Vehicle

    // === ViewModel ===
    private val vehiclesViewModel by lazy {
        ViewModelProvider(requireActivity())[VehiclesViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentVehicleBinding.inflate(inflater, container, false)
        val root: View = binding.root

        vehicleSource = arguments?.getSerializable("vehicle") as Vehicle

        val refuels = vehiclesViewModel.getRefuelsOf(vehicleSource)
        vehicleSource.refuels = refuels

        val bundle = bundleOf("vehicle" to vehicleSource)
        parentFragmentManager.commit {
            setReorderingAllowed(true)
            add<VehiclePartFragment>(R.id.vehicleFragment, args = bundle)
        }

        // === Set UI values ===
        if (vehicleSource.refuels.isEmpty()) {
            binding.noRefuel.visibility = View.VISIBLE
            binding.haveRefuels.visibility = View.GONE
        } else {
            binding.noRefuel.visibility = View.GONE
            binding.haveRefuels.visibility = View.VISIBLE
            createTable(vehicleSource.refuels)
        }

        // === Actions ===
        binding.refuelsArea.apply {
            for (i in 1 until this.childCount) {
                val row = this.getChildAt(i) as TableRow
                val refuel = row.tag as Refuel
                row.setOnClickListener {
                    val bundle = bundleOf(
                        "vehicle" to vehicleSource,
                        "refuel" to refuel
                    )
                    NavHostFragment.findNavController(requireParentFragment()).navigate(R.id.action_vehicleFragment_to_refuelFragment, bundle)
                }
            }
        }

        binding.btnAddRefuel.setOnClickListener {
            val bundle = bundleOf("vehicle" to vehicleSource)
            NavHostFragment.findNavController(requireParentFragment()).navigate(R.id.action_vehicleFragment_to_refuelEditFragment, bundle)
        }

        return root
    }

    override fun onResume() {
        super.onResume()

        if (vehiclesViewModel.getVehicles().find { it === vehicleSource } == null) {
            // Vehicle was deleted, go back one more view (to the "Vehicles" fragment)
            NavHostFragment.findNavController(this).navigateUp()
        }
    }

    private fun createTable(refuels: List<Refuel>) {
        val sortedRefuels = refuels.sortedByDescending { it.date }

        val dateFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(Locale.getDefault());

        for ((counter, refuel) in sortedRefuels.withIndex()) {
            val row = TableRow(context)

            // use primary color for odd rows
            if (counter % 2 == 0) {
                row.setBackgroundColor(resources.getColor(R.color.secondaryLightColor, null))
            } else {
                row.setBackgroundColor(resources.getColor(R.color.secondaryColor, null))
            }

            row.layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT)

            addTextViewCell(row, refuel.date.format(dateFormatter))
            addTextViewCell(row, DecimalFormat("#.##").format(refuel.totalPrice))

            row.tag = refuel

            binding.refuelsArea.addView(row)
        }
    }

    private fun addTextViewCell(row: TableRow, text: String) {
        val cell = TextView(context)
        cell.text = text
        val horizontalPadding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16f, resources.displayMetrics).toInt()
        val verticalPadding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8f, resources.displayMetrics).toInt()
        cell.setPadding(horizontalPadding, verticalPadding, horizontalPadding, verticalPadding)

        cell.setTextColor(resources.getColor(R.color.secondaryTextColor, null))

        cell.textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 4f, resources.displayMetrics)

        row.addView(cell)
    }
}