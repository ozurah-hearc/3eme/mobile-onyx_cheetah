package ch.hearc.onyxcheetah.ui.refuels

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import ch.hearc.onyxcheetah.R
import ch.hearc.onyxcheetah.data.Refuel
import ch.hearc.onyxcheetah.data.Vehicle
import ch.hearc.onyxcheetah.databinding.FragmentRefuelBinding
import ch.hearc.onyxcheetah.utils.BitmapUtils
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.mapbox.geojson.Point
import com.mapbox.maps.MapView
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.Style
import com.mapbox.maps.plugin.annotation.annotations
import com.mapbox.maps.plugin.annotation.generated.PointAnnotationOptions
import com.mapbox.maps.plugin.annotation.generated.createPointAnnotationManager
import com.mapbox.maps.plugin.gestures.gestures
import java.text.DecimalFormat
import java.text.NumberFormat
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class RefuelFragment : Fragment() {
    private lateinit var binding: FragmentRefuelBinding

    private lateinit var refuelSource: Refuel
    private lateinit var vehicleSource: Vehicle
    private lateinit var mapView: MapView
    private lateinit var map: MapboxMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRefuelBinding.inflate(inflater, container, false)
        val root: View = binding.root

        vehicleSource = arguments?.getSerializable("vehicle") as Vehicle
        refuelSource = arguments?.getSerializable("refuel") as Refuel

        // === Set UI values ===
        binding.vehicle.text = vehicleSource.name
        binding.date.text =
            refuelSource.date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG))

        binding.image.visibility = if (refuelSource.photoUri != null) View.VISIBLE else View.GONE

        binding.traveledDistance.text = DecimalFormat("0.00").format(refuelSource.km)

        binding.refuelType.text = getString(refuelSource.fuelType.value)
        binding.liters.text = DecimalFormat("0.00").format(refuelSource.liters)
        binding.pricePerLiter.text =
            NumberFormat.getCurrencyInstance().format(refuelSource.pricePerLiter)
        binding.totalPrice.text = NumberFormat.getCurrencyInstance().format(refuelSource.totalPrice)

        // map init
        mapView = binding.mapView
        mapView.getMapboxMap().loadStyleUri(Style.MAPBOX_STREETS)
        mapView.gestures.pinchToZoomEnabled = true
        map = mapView.getMapboxMap()
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        val refuelPoint = Point.fromLngLat(refuelSource.longitude ?: 0.0, refuelSource.latitude ?: 0.0)
        // annotation at center
        val annotationApi = mapView.annotations
        val annotationManager = annotationApi.createPointAnnotationManager()
        annotationManager.create(
            PointAnnotationOptions().withPoint(refuelPoint).withIconImage(BitmapUtils.bitmapFromDrawableRes(requireContext(), R.drawable.red_marker)!!)
        )

        // place camera on annotation
        map.setCamera(
            com.mapbox.maps.CameraOptions.Builder()
                .center(refuelPoint)
                .zoom(15.0)
                .build()
        )

        val uri = refuelSource.photoUri?.let { Uri.parse(it) }
        binding.image.setImageURI(uri)

        // === Actions ===
        binding.btnEdit.setOnClickListener {
            onEdit()
        }

        return root
    }

    override fun onResume() {
        super.onResume()

        if (vehicleSource.refuels.find { it === refuelSource } == null) {
            // Refuel was deleted or moved to another vehicle, go back one more view (to the "Vehicle" fragment)
            NavHostFragment.findNavController(this).navigateUp()
        }
    }

    private fun onEdit() {
        val bundle = bundleOf(
            "vehicle" to vehicleSource,
            "refuel" to refuelSource
        )
        NavHostFragment.findNavController(this)
            .navigate(R.id.action_refuelFragment_to_refuelEditFragment, bundle)
    }
}