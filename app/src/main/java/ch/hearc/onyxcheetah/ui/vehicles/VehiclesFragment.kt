package ch.hearc.onyxcheetah.ui.vehicles

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.DividerItemDecoration
import ch.hearc.onyxcheetah.R
import ch.hearc.onyxcheetah.data.Vehicle
import ch.hearc.onyxcheetah.viewModel.VehiclesViewModel
import ch.hearc.onyxcheetah.databinding.FragmentVehiclesBinding
import ch.hearc.onyxcheetah.enums.FuelType
import ch.hearc.onyxcheetah.ui.parts.adapters.VehiclesAdapter
import ch.hearc.onyxcheetah.utils.MarginItemDecoration

class VehiclesFragment : Fragment() {
    private lateinit var binding: FragmentVehiclesBinding

    private val vehiclesViewModel by lazy {
        ViewModelProvider(requireActivity())[VehiclesViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentVehiclesBinding.inflate(inflater, container, false)
        val root: View = binding.root

        // Prepare the RecyclerView
        val adapter = VehiclesAdapter(this)
        val rv = binding.vehiclesRecyclerView
        rv.adapter = adapter
        rv.addItemDecoration(DividerItemDecoration(rv.context, DividerItemDecoration.VERTICAL))

        // Observe (update) the RecyclerView when the list of vehicles changes
        vehiclesViewModel.vehicles.observe(viewLifecycleOwner) { results ->
            adapter.setList(results)
        }

        val btnAdd = binding.addVehicleButton
        btnAdd.setOnClickListener {
            addNewVehicleView()
        }

        return root
    }

    private fun addNewVehicleView() {
        NavHostFragment.findNavController(this).navigate(R.id.action_nav_cars_to_vehicleEditFragment)
    }
}