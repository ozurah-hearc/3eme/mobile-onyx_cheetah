package ch.hearc.onyxcheetah.ui.refuels

import android.Manifest
import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import ch.hearc.onyxcheetah.R
import ch.hearc.onyxcheetah.adapter.FuelTypeSpinnerAdapter
import ch.hearc.onyxcheetah.adapter.VehicleSpinnerAdapter
import ch.hearc.onyxcheetah.data.Refuel
import ch.hearc.onyxcheetah.data.Vehicle
import ch.hearc.onyxcheetah.databinding.FragmentRefuelEditBinding
import ch.hearc.onyxcheetah.enums.FuelType
import ch.hearc.onyxcheetah.utils.BitmapUtils
import ch.hearc.onyxcheetah.utils.ImagePicker
import ch.hearc.onyxcheetah.utils.SimpleAlertDialog
import ch.hearc.onyxcheetah.viewModel.VehiclesViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.mapbox.geojson.Point
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.MapView
import com.mapbox.maps.MapboxMap
import com.mapbox.maps.Style
import com.mapbox.maps.plugin.annotation.annotations
import com.mapbox.maps.plugin.annotation.generated.PointAnnotationOptions
import com.mapbox.maps.plugin.annotation.generated.createPointAnnotationManager
import com.mapbox.maps.plugin.gestures.gestures
import fr.quentinklein.slt.LocationTracker
import fr.quentinklein.slt.ProviderError
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*

class RefuelEditFragment : Fragment() {
    private lateinit var binding: FragmentRefuelEditBinding

    private lateinit var refuelSource: Refuel
    private var vehicleSource: Vehicle? = null
    private lateinit var selectedVehicle: Vehicle
    private var isNew: Boolean = false
    private var imagePicker: ImagePicker? = null
    private var photoUri: Uri? = null
    private lateinit var mapView: MapView;
    private lateinit var map: MapboxMap;
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    // === ViewModel ===
    private val vehiclesViewModel by lazy {
        ViewModelProvider(requireActivity())[VehiclesViewModel::class.java]
    }
    // === END === ViewModel ===

    private val dateFormatter: DateTimeFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(Locale.US)
    private val numberFormatter: DecimalFormat = DecimalFormat("0.00", DecimalFormatSymbols(Locale.US))

    private lateinit var tempDate: LocalDate

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        imagePicker = ImagePicker(this, ::onImagePicked)
    }
    
    private val locationTracker = LocationTracker(
        5 * 60 * 1000.toLong(),
        100f,
        true,
        shouldUseNetwork = true,
        shouldUsePassive = true
    )


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRefuelEditBinding.inflate(inflater, container, false)
        val root: View = binding.root

        // map init
        mapView = binding.mapView
        mapView.getMapboxMap().loadStyleUri(Style.MAPBOX_STREETS)
        mapView.gestures.pinchToZoomEnabled = true;
        map = mapView.getMapboxMap()
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        locationTracker.addListener(object: LocationTracker.Listener {
            override fun onLocationFound(location: Location) {
                Log.d("LocationTracker", "onLocationFound: $location")
                map.setCamera(
                    CameraOptions.Builder()
                        .center(Point.fromLngLat(location.longitude, location.latitude))
                        .zoom(15.0)
                        .build()
                )
                locationTracker.stopListening()
            }

            override fun onProviderError(providerError: ProviderError) {
                Log.e("LocationTracker", "onProviderError: $providerError")
            }
        });

        // annotation at center
        val annotationApi = mapView.annotations
        val annotationManager = annotationApi.createPointAnnotationManager()
        annotationManager.create(
            PointAnnotationOptions().withPoint(map.cameraState.center).withIconImage(BitmapUtils.bitmapFromDrawableRes(requireContext(), R.drawable.red_marker)!!)
        )

        map.addOnCameraChangeListener {
            val currentPoint = annotationManager.annotations.first()
            currentPoint.point = map.cameraState.center
            annotationManager.update(currentPoint)
        }

        numberFormatter.isParseBigDecimal = true
        numberFormatter.isGroupingUsed = false

        // === Security to avoid crash ===
        if (vehiclesViewModel.getVehicles().isEmpty()) {
            // We create a new vehicle, this is less annoying for the user to redirect to the
            // "create vehicle" first and then becomes back to the refuel creation
            val newVehicle = Vehicle(getString(R.string.new_vehicle_default_name))

            vehiclesViewModel.updateVehicle(newVehicle)
            vehiclesViewModel.getVehicles().add(newVehicle)

            Log.i(
                "CRUD",
                "Vehicle ${newVehicle.name} created"
            )
            Toast.makeText(context, R.string.info_vehicle_create, Toast.LENGTH_SHORT).show()
        }

        // === Get data ===
        vehicleSource = arguments?.getSerializable("vehicle") as? Vehicle ?: vehiclesViewModel.getVehicles()[0]

        refuelSource = arguments?.getSerializable("refuel") as? Refuel ?: Refuel()

        isNew = arguments == null || arguments?.containsKey("refuel") == false

        tempDate = refuelSource.date

        // === Set UI values ===
        binding.btnToday.text = refuelSource.date.format(dateFormatter)

        photoUri = refuelSource.photoUri?.let { Uri.parse(it) }
        binding.image.setImageURI(photoUri)

        binding.sListVehicles.adapter =
            VehicleSpinnerAdapter(requireContext(), vehiclesViewModel.getVehicles())
        selectedVehicle = vehicleSource ?: vehiclesViewModel.getVehicles()[0]

        binding.sListFuelType.adapter = FuelTypeSpinnerAdapter(requireContext(), resources.getStringArray(R.array.fuel_types).toList())

        binding.traveledDistance.setText(DecimalFormat("0.00").format(refuelSource.km))

        if (vehicleSource != null) {
            val selectedVehicle = vehiclesViewModel.getVehicles().indexOfFirst { selectedVehicle === it }
            binding.sListVehicles.setSelection(selectedVehicle)

            val selIndex = resources.getStringArray(R.array.fuel_types)
                .indexOf(getString(vehicleSource!!.fuelType.value))
            binding.sListFuelType.setSelection(selIndex)

        }

        if(refuelSource.latitude != null && refuelSource.longitude != null) {
            map.setCamera(CameraOptions.Builder()
                .center(Point.fromLngLat(refuelSource.longitude!!, refuelSource.latitude!!))
                .zoom(9.0)
                .build())
        } else {
            localise()
        }

        binding.liters.setText(DecimalFormat("0.00").format(refuelSource.liters))
        binding.pricePerLiter.setText(DecimalFormat("0.00").format(refuelSource.pricePerLiter))
        binding.totalPrice.setText(DecimalFormat("0.00").format(refuelSource.totalPrice))

        binding.btnDelete.visibility = if (isNew) View.GONE else View.VISIBLE

        // === Actions ===

        binding.sListVehicles.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            // non-null is null fix https://stackoverflow.com/questions/47849219/java-lang-illegalargumentexception-parameter-specified-as-non-null-is-null-me
            override fun onItemSelected(
                parent: AdapterView<*>?, view: View?, position: Int, id: Long
            ) {
                selectedVehicle = view?.tag as Vehicle? ?: vehiclesViewModel.getVehicles()[0]

                // if isn't new, it's an existing refuel with all of it's set data, so we don't want to update
                // the fuel type. (Else the user will not necessary notice the change and it's wasn't good)
                if (isNew) {
                    binding.sListFuelType.setSelection(
                        resources.getStringArray(R.array.fuel_types)
                            .indexOf(getString(selectedVehicle.fuelType.value))
                    )
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // Do nothing
            }
        }

        // Radio Group with auto calculation

        binding.liters.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                val liters = stringToBigDecimal(binding.liters.text.toString())
                val pricePerLiter = stringToBigDecimal(binding.pricePerLiter.text.toString())
                binding.totalPrice.setText(
                    numberFormatter.format(liters.times(pricePerLiter))
                )
            }
        }

        binding.pricePerLiter.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                val liters = stringToBigDecimal(binding.liters.text.toString())
                val pricePerLiter = stringToBigDecimal(binding.pricePerLiter.text.toString())
                binding.totalPrice.setText(
                    numberFormatter.format(liters.times(pricePerLiter))
                )
            }
        }

        binding.totalPrice.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {

                val totalPrice = stringToBigDecimal(binding.totalPrice.text.toString())
                val liters = stringToBigDecimal(binding.liters.text.toString())
                val pricePerLiter = stringToBigDecimal(binding.pricePerLiter.text.toString())

                if(liters != BigDecimal.ZERO) {
                    binding.pricePerLiter.setText(
                        numberFormatter.format(totalPrice.divide(liters, 2, RoundingMode.FLOOR))
                    )
                } else if (pricePerLiter != BigDecimal.ZERO) {
                    binding.liters.setText(
                        numberFormatter.format(totalPrice.divide(pricePerLiter, 2, RoundingMode.FLOOR))
                    )
                }
            }
        }

        // == Buttons ==
        binding.btnToday.setOnClickListener {
            onDatePick()
        }

        binding.btnChangeImage.setOnClickListener {
            onChangeImage()
        }

        binding.btnRemoveImage.setOnClickListener {
            onRemoveImage()
        }

        binding.btnValidate.setOnClickListener {
            onValidate()
        }

        binding.btnDelete.setOnClickListener {
            onDelete()
        }

        binding.btnLocaliseMe.setOnClickListener {
            localise()
        }

        return root
    }

    private fun onDatePick() {
        val year = refuelSource.date.year
        val month = refuelSource.date.monthValue - 1
        val day = refuelSource.date.dayOfMonth

        val datePickerDialog = DatePickerDialog(
            requireContext(),
            { _, yearIn, monthIn, dayIn ->
                tempDate = LocalDate.of(yearIn, monthIn + 1, dayIn)
                binding.btnToday.text = tempDate.format(dateFormatter)
            },
            year,
            month,
            day
        )
        datePickerDialog.show()
    }

    private fun onChangeImage() {
        imagePicker?.askUserForImage()
    }

    private fun onImagePicked(uri: Uri?) {
        photoUri = uri
        binding.image.setImageURI(uri)
    }

    private fun onRemoveImage() {
        photoUri = null
        binding.image.setImageURI(null)
    }

    private fun onValidate() {
        // Update the refuel source with the data from the UI
        refuelSource.date = tempDate
        refuelSource.km = stringToBigDecimal(binding.traveledDistance.text.toString())
        
        refuelSource.photoUri = photoUri?.toString()

        refuelSource.fuelType = FuelType.values()[binding.sListFuelType.selectedItemPosition]
        refuelSource.totalPrice = stringToBigDecimal(binding.totalPrice.text.toString())
        refuelSource.liters = stringToBigDecimal(binding.liters.text.toString())
        refuelSource.pricePerLiter = stringToBigDecimal(binding.pricePerLiter.text.toString())
        refuelSource.latitude = map.cameraState.center.latitude()
        refuelSource.longitude = map.cameraState.center.longitude()

        vehiclesViewModel.updateRefuelOfVehicle(refuelSource, selectedVehicle)

        // Link the refuel to the correct vehicle
        if (isNew) {
            vehiclesViewModel.getVehicles()
                .first { vehicle -> vehicle === selectedVehicle }.refuels.add(refuelSource)
            Log.i(
                "CRUD",
                "Refuel using ${refuelSource.fuelType} at ${refuelSource.date} created"
            )
            Toast.makeText(context, R.string.info_refuel_create, Toast.LENGTH_SHORT).show()
            isNew = false

            NavHostFragment.findNavController(this).navigateUp()

            return
        }

        if (vehicleSource !== selectedVehicle) {
            // Update the owner
            vehiclesViewModel.getVehicles()
                .first { vehicle -> vehicle === selectedVehicle }.refuels.add(refuelSource)
            vehicleSource?.refuels?.removeIf { refuel -> refuel === refuelSource }
        }

        Log.i(
            "CRUD",
            "Refuel using ${refuelSource.fuelType} at ${refuelSource.date} updated"
        )
        Toast.makeText(context, R.string.info_refuel_update, Toast.LENGTH_SHORT).show()

        NavHostFragment.findNavController(this).navigateUp()
    }

    private fun onDelete() {
        SimpleAlertDialog.show(
            requireContext(),
            onOk = {
                vehicleSource?.refuels?.removeIf { refuel -> refuel === refuelSource }
                vehiclesViewModel.deleteRefuel(refuelSource)

                NavHostFragment.findNavController(this).navigateUp()
                Log.i(
                    "CRUD",
                    "Refuel using ${refuelSource.fuelType} at ${refuelSource.date} deleted"
                )
                Toast.makeText(context, R.string.info_refuel_delete, Toast.LENGTH_SHORT).show()
            })
    }

    private fun stringToBigDecimal(str: String): BigDecimal {
        return if (str.isEmpty()) BigDecimal.ZERO else numberFormatter.parse(str) as BigDecimal
    }

    private fun localise() {
        binding.btnLocaliseMe.isClickable = false
        binding.btnLocaliseMe.isEnabled = false
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // Runtime permission request source : https://stackoverflow.com/questions/40142331/how-to-request-location-permission-at-runtime
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    requireActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION
                )
            ) {
                Log.d("localisation", "request rationale")
                AlertDialog.Builder(requireContext())
                    .setTitle("Localisation nécessaire")
                    .setMessage("Cette action requiert de connaître votre localisation, merci d'autoriser l'accès à la localisation.")
                    .setPositiveButton(
                        "OK"
                    ) { _, _ ->
                        requestLocationPermission()
                    }
                    .create()
                    .show()
            } else {
                Log.d("localisation", "requestLocationPermission")
                requestLocationPermission()
            }
        } else {
            executeLocalisation()
        }
        binding.btnLocaliseMe.isClickable = true
        binding.btnLocaliseMe.isEnabled = true
    }

    @SuppressLint("MissingPermission")
    private fun executeLocalisation() {
        binding.btnLocaliseMe.isClickable = false
        binding.btnLocaliseMe.isEnabled = false

        locationTracker.startListening(requireContext())
    }


    private fun requestLocationPermission() {
        ActivityCompat.OnRequestPermissionsResultCallback { requestCode, _, grantResults ->
            if (requestCode == REQUEST_PERMISSION_CODE) {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    executeLocalisation()
                } else {
                    Toast.makeText(
                        context,
                        "La localisation a été refusée.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }

        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
            ),
            REQUEST_PERMISSION_CODE
        )
    }


    companion object {
        private const val REQUEST_PERMISSION_CODE = 42
    }
}