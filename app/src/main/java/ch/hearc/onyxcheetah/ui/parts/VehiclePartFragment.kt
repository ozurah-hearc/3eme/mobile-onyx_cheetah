// ========================== REMARK ==========================
// The code is nearly the same as the one in the VehiclesAdapter class
// TODO : Check if it's possible to avoid code duplication ?

package ch.hearc.onyxcheetah.ui.parts

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import ch.hearc.onyxcheetah.R
import ch.hearc.onyxcheetah.data.Vehicle
import ch.hearc.onyxcheetah.databinding.PartVehicleBinding

class VehiclePartFragment : Fragment() {

    private lateinit var binding: PartVehicleBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = PartVehicleBinding.inflate(inflater)
        val root: View = binding.root

        val vehicle = arguments?.getSerializable("vehicle") as Vehicle

        // === Display data ===
        binding.vehicleName.text = vehicle.name
        // Get the R.string value from the enum + get it's string value
        binding.fuelType.text = getString(vehicle.fuelType.value)
        val uri = vehicle.imageUri?.let { Uri.parse(it) }
        binding.vehiclePreviewImage.setImageURI(uri)
        //binding.btnPerform.text = getString(R.string.btnEdit)

        // === Set actions ===
        binding.btnPerform.setImageResource(R.drawable.edit_48px)
        binding.btnPerform.setOnClickListener {
            val bundle = bundleOf("vehicle" to vehicle)

            NavHostFragment.findNavController(this).navigate(R.id.action_vehicleFragment_to_vehicleEditFragment, bundle)
        }

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}