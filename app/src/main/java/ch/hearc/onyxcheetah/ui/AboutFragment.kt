package ch.hearc.onyxcheetah.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import ch.hearc.onyxcheetah.R
import ch.hearc.onyxcheetah.databinding.FragmentAboutBinding
import ch.hearc.onyxcheetah.viewModel.AboutViewModel

class AboutFragment : Fragment() {
    private lateinit var binding: FragmentAboutBinding

    private val aboutViewModel by lazy {
        ViewModelProvider(requireActivity())[AboutViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAboutBinding.inflate(inflater, container, false)
        val root: View = binding.root


        binding.authorAllemannJ.setOnClickListener { openUrl(R.string.url_allemannJ) }
        binding.authorChappuisS.setOnClickListener { openUrl(R.string.url_chappuisS) }
        binding.authorStouderX.setOnClickListener { openUrl(R.string.url_stouderX) }

        binding.heArc.setOnClickListener { openUrl(R.string.url_hearc) }
        binding.imageHeArc.setOnClickListener { openUrl(R.string.url_hearc) }

        return root
    }

    private fun openUrl(url: Int) {
        openUrl(getString(url))
    }

    private fun openUrl(url: String) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(browserIntent)
    }
}