package ch.hearc.onyxcheetah.ui.parts.adapters

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.RecyclerView
import ch.hearc.onyxcheetah.R
import ch.hearc.onyxcheetah.data.Vehicle
import ch.hearc.onyxcheetah.databinding.PartVehicleBinding

class VehiclesAdapter(private val fragment: Fragment) :
    RecyclerView.Adapter<VehiclesAdapter.VehicleViewHolder>() {

    private lateinit var binding: PartVehicleBinding

    private var vehicles = emptyList<Vehicle>()

    inner class VehicleViewHolder(item: View) : RecyclerView.ViewHolder(item) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VehicleViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        binding = PartVehicleBinding.inflate(layoutInflater, parent, false)
        return VehicleViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: VehicleViewHolder, position: Int) {
        var vehicle = vehicles[position]

        // === Display data ===
        binding.vehicleName.text = vehicle.name
        // Get the R.string value from the enum + get it's string value
        binding.fuelType.text = fragment.getString(vehicle.fuelType.value)
        val uri = vehicle.imageUri?.let { Uri.parse(it) }
        binding.vehiclePreviewImage.setImageURI(uri)
        // getString required, else we will got a null value
        //binding.btnPerform.text = fragment.getString(R.string.btnShow)

        // === Set actions ===
        binding.btnPerform.setOnClickListener {
            val bundle = bundleOf("vehicle" to vehicle)

            NavHostFragment.findNavController(fragment).navigate(R.id.action_nav_cars_to_vehicleFragment, bundle)
        }
    }

    override fun getItemCount(): Int {
        return vehicles.size
    }

    fun setList(vehicles: List<Vehicle>) {
        this.vehicles = vehicles
        notifyDataSetChanged()
    }
}