package ch.hearc.onyxcheetah.ui.refuels

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TableRow
import android.widget.TextView
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import ch.hearc.onyxcheetah.R
import ch.hearc.onyxcheetah.data.Refuel
import ch.hearc.onyxcheetah.data.Vehicle
import ch.hearc.onyxcheetah.databinding.FragmentNavRefuelBinding
import ch.hearc.onyxcheetah.enums.FuelType
import ch.hearc.onyxcheetah.ui.parts.VehiclePartFragment
import ch.hearc.onyxcheetah.utils.SimpleAlertDialog
import ch.hearc.onyxcheetah.viewModel.VehiclesViewModel

/**
 * Fragment used for the refuels navigation, it contains a simple fragment to display the main content.
 * We use it to display the "Add a new refuel".
 */
class NavRefuelFragment : Fragment()  {
    private lateinit var binding: FragmentNavRefuelBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNavRefuelBinding.inflate(inflater, container, false)
        val root: View = binding.root

        parentFragmentManager.commit {
            setReorderingAllowed(true)
            add<RefuelEditFragment>(R.id.fragment)
        }

        return root;
    }
}