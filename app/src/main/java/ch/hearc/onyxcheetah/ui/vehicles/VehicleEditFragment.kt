package ch.hearc.onyxcheetah.ui.vehicles

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import ch.hearc.onyxcheetah.R
import ch.hearc.onyxcheetah.adapter.FuelTypeSpinnerAdapter
import ch.hearc.onyxcheetah.data.Vehicle
import ch.hearc.onyxcheetah.databinding.FragmentVehicleEditBinding
import ch.hearc.onyxcheetah.enums.FuelType
import ch.hearc.onyxcheetah.utils.ImagePicker
import ch.hearc.onyxcheetah.utils.SimpleAlertDialog
import ch.hearc.onyxcheetah.viewModel.VehiclesViewModel


class VehicleEditFragment : Fragment() {
    private lateinit var binding: FragmentVehicleEditBinding

    private lateinit var vehicleSource: Vehicle
    private var isNew: Boolean = false
    private var imagePicker: ImagePicker? = null
    private var imageUri: Uri? = null

    private val vehiclesViewModel by lazy {
        ViewModelProvider(requireActivity())[VehiclesViewModel::class.java]
    }
    // === END === ViewModel ===

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        imagePicker = ImagePicker(this, ::onImagePicked)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentVehicleEditBinding.inflate(inflater, container, false)
        val root: View = binding.root

        vehicleSource = arguments?.getSerializable("vehicle") as? Vehicle ?: Vehicle(
            getString(R.string.new_vehicle_default_name),
            FuelType.SUPER95
        )

        isNew = arguments == null || arguments?.containsKey("vehicle") == false

        // === Set UI values ===
        binding.editTxtName.setText(vehicleSource.name)
        imageUri  = vehicleSource.imageUri?.let { Uri.parse(it) }
        binding.image.setImageURI(imageUri)
        val selIndex = resources.getStringArray(R.array.fuel_types)
            .indexOf(getString(vehicleSource.fuelType.value))
        binding.sListFuelType.setSelection(selIndex)

        binding.sListFuelType.adapter = FuelTypeSpinnerAdapter(requireContext(), resources.getStringArray(R.array.fuel_types).toList())

        // Visibility
        binding.btnDelete.visibility = if (isNew) View.GONE else View.VISIBLE

        // === Button Actions ===
        binding.btnChangeImage.setOnClickListener {
            onChangeImage()
        }

        binding.btnValidate.setOnClickListener {
            onValidate()
        }

        binding.btnDelete.setOnClickListener {
            onDelete()
        }

        return root
    }

    private fun onChangeImage() {
        imagePicker?.askUserForImage()
    }

    private fun onImagePicked(uri: Uri) {
        imageUri = uri
        binding.image.setImageURI(uri)
    }

    private fun onValidate() {
        vehicleSource.imageUri = imageUri?.toString()
        vehicleSource.name = binding.editTxtName.text.toString()

        vehicleSource.fuelType =
            FuelType.values().find { getString(it.value) == binding.sListFuelType.selectedItem }!!

        vehiclesViewModel.updateVehicle(vehicleSource)

        // If it's a new vehicle, add it to the VM
        if (isNew) {
            vehiclesViewModel.getVehicles().add(vehicleSource)

            Log.i(
                "CRUD",
                "Vehicle ${vehicleSource.name} created"
            )
            Toast.makeText(context, R.string.info_vehicle_create, Toast.LENGTH_SHORT).show()
            isNew = false

            NavHostFragment.findNavController(this).navigateUp()

            return
        }
        // else if vehicle already exists, ViewModel will be updated automatically

        Log.i(
            "CRUD",
            "Vehicle ${vehicleSource.name} updated"
        )
        Toast.makeText(context, R.string.info_vehicle_update, Toast.LENGTH_SHORT).show()

        NavHostFragment.findNavController(this).navigateUp()
    }

    private fun onDelete() {
        SimpleAlertDialog.show(
            requireContext(),
            onOk = {
                vehiclesViewModel.getVehicles().removeIf {it === vehicleSource}
                vehiclesViewModel.deleteVehicle(vehicleSource)

                NavHostFragment.findNavController(this).navigateUp()
                Log.i(
                    "CRUD",
                    "Vehicle ${vehicleSource.name} deleted"
                )
                Toast.makeText(context, R.string.info_vehicle_delete, Toast.LENGTH_SHORT).show()
            })
    }
}
