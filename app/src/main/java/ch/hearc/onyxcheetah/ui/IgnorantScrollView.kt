package ch.hearc.onyxcheetah.ui

import android.graphics.Rect
import android.view.MotionEvent
import android.view.View
import android.widget.ScrollView
import ch.hearc.onyxcheetah.R
import com.mapbox.maps.MapView

class IgnorantScrollView : ScrollView {
    var ignored: MapView? = null

    constructor(context: android.content.Context) : super(context)
    constructor(context: android.content.Context, attrs: android.util.AttributeSet) : super(context, attrs)
    constructor(context: android.content.Context, attrs: android.util.AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun onInterceptTouchEvent(motionEvent: MotionEvent?): Boolean {
        if(ignored == null) {
            ignored = findViewById(R.id.mapView)
        }
        if (isOnIgnored(ignored, motionEvent)) {
            return false
        }
        return super.onInterceptTouchEvent(motionEvent)
    }

    private fun isOnIgnored(view: View?, motionEvent: MotionEvent?): Boolean {
        if (motionEvent == null) {
            return false
        }
        val hitBox = Rect()
        ignored?.getGlobalVisibleRect(hitBox)
        return hitBox.contains(motionEvent.rawX.toInt(), motionEvent.rawY.toInt())
    }
}