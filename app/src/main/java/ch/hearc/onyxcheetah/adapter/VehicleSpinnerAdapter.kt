package ch.hearc.onyxcheetah.adapter

import android.content.Context
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import ch.hearc.onyxcheetah.data.Vehicle

class VehicleSpinnerAdapter(
    context: Context, private val objects: List<Vehicle>,
    dropDownItem: Int = android.R.layout.simple_spinner_dropdown_item
) :
    ArrayAdapter<Vehicle>(context, dropDownItem, objects) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = super.getView(position, convertView, parent) as TextView
        view.tag = getItem(position)
        view.text = getItem(position)?.name ?: "[UNKNOWN VEHICLE]"
        view.textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 6f, context.resources.displayMetrics)
        return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = super.getDropDownView(position, convertView, parent) as TextView
        view.tag = getItem(position)
        view.text = getItem(position)?.name ?: "[UNKNOWN VEHICLE]"
        view.textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 6f, context.resources.displayMetrics)
        return view
    }
}