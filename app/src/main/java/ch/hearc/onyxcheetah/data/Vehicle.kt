package ch.hearc.onyxcheetah.data

import android.net.Uri
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import ch.hearc.onyxcheetah.R
import ch.hearc.onyxcheetah.enums.FuelType
import java.io.Serializable
import java.math.BigDecimal
import java.math.RoundingMode

@Entity // Will create a table named "Vehicle"
data class Vehicle(
    var name: String = "new vehicle",
    var fuelType: FuelType = FuelType.SUPER98,
) : Serializable {
    @PrimaryKey(autoGenerate = true)
    var vehicleId: Long = 0

    @Ignore
    var refuels: MutableList<Refuel> = mutableListOf()
    var imageUri: String? = null

    constructor(vehicle: Vehicle) : this(vehicle.name, vehicle.fuelType) {
        refuels = vehicle.refuels.map { Refuel(it) } as MutableList<Refuel>
        imageUri = vehicle.imageUri
    }

    // Call this when adding a new refuel to the vehicle without using the DAO
    fun linkRefuelsToVehicle() {
        for (refuel in refuels) {
            refuel.forVehicleId = vehicleId
        }
    }

    fun getRealConsumption(): BigDecimal {
        if (refuels.size < 2) {
            return BigDecimal.ZERO
        }
        val lastRefuel = refuels[refuels.size - 1]
        val lastRefuelBefore = refuels[refuels.size - 2]
        val km = lastRefuel.km.minus(lastRefuelBefore.km)
        val liters = lastRefuel.liters
        return km.divide(liters, 2, RoundingMode.FLOOR)
    }

    fun getAverageConsumption(): BigDecimal {
        if (refuels.size < 2) {
            return BigDecimal.ZERO
        }
        var totalKm = BigDecimal.ZERO
        var totalLiters = BigDecimal.ZERO
        for (i in 1 until refuels.size) {
            val lastRefuel = refuels[i]
            val lastRefuelBefore = refuels[i - 1]
            totalKm = totalKm.plus(lastRefuel.km.minus(lastRefuelBefore.km))
            totalLiters = totalLiters.plus(lastRefuel.liters)
        }
        return totalKm.divide(totalLiters, 2, RoundingMode.FLOOR)
    }

    fun getAveragePricePerLiter(): BigDecimal {
        if (refuels.size < 2) {
            return BigDecimal.ZERO
        }
        var total = BigDecimal.ZERO
        for (refuel in refuels) {
            total = total.plus(refuel.pricePerLiter)
        }
        return total.divide(BigDecimal(refuels.size), 2, RoundingMode.FLOOR)
    }
}
