package ch.hearc.onyxcheetah.data

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.Companion.CASCADE
import androidx.room.PrimaryKey
import ch.hearc.onyxcheetah.enums.AutoFuelCalc
import ch.hearc.onyxcheetah.enums.FuelType
import java.io.Serializable
import java.math.BigDecimal
import java.math.RoundingMode
import java.time.LocalDate

@Entity(
    foreignKeys = [
        ForeignKey(
            entity = Vehicle::class,
            parentColumns = ["vehicleId"],
            childColumns = ["forVehicleId"],
            onDelete = CASCADE
        )
    ]
)
data class Refuel(
    var km: BigDecimal = BigDecimal.ZERO,
    var fuelType: FuelType = FuelType.SUPER98,
) : Serializable
{
    @PrimaryKey(autoGenerate = true)
    var refuelId: Long = 0

    var forVehicleId: Long = 0
    var date: LocalDate = LocalDate.now()
    var pricePerLiter: BigDecimal = BigDecimal.ZERO
    var liters: BigDecimal = BigDecimal.ZERO
    var totalPrice: BigDecimal = BigDecimal.ZERO
    var photoUri: String? = null
    var latitude: Double? = null
    var longitude: Double? = null

    /**
     * Copy constructor (deep copy)
     */
    constructor(refuel: Refuel) : this(refuel.km, refuel.fuelType) {
        forVehicleId = refuel.forVehicleId
        date = refuel.date
        pricePerLiter = refuel.pricePerLiter
        liters = refuel.liters
        totalPrice = refuel.totalPrice
        photoUri = refuel.photoUri
        latitude = refuel.latitude
        longitude = refuel.longitude
    }

    private fun calcPricePerLiter(): BigDecimal {
        if (liters == BigDecimal.ZERO) {
            return BigDecimal.ZERO
        }
        return totalPrice.divide(liters, 2, RoundingMode.FLOOR)
    }

    private fun calcLiters(): BigDecimal {
        if (pricePerLiter == BigDecimal.ZERO) {
            return BigDecimal.ZERO
        }
        return totalPrice.divide(pricePerLiter, 2, RoundingMode.FLOOR)
    }

    private fun calcTotalPrice(): BigDecimal {
        return liters.multiply(pricePerLiter)
    }

    fun calcMissingValue(auto: AutoFuelCalc) {
        when (auto) {
            AutoFuelCalc.LITERS -> liters = calcLiters()
            AutoFuelCalc.PRICE_PER_LITER -> pricePerLiter = calcPricePerLiter()
            AutoFuelCalc.TOTAL_PRICE -> totalPrice = calcTotalPrice()
        }
    }
}
