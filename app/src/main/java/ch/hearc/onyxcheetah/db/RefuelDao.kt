package ch.hearc.onyxcheetah.db

import androidx.lifecycle.LiveData
import androidx.room.*
import ch.hearc.onyxcheetah.data.Refuel

@Dao
interface RefuelDao {
    @Query("SELECT * FROM refuel")
    fun getAll(): LiveData<MutableList<Refuel>>

    @Query("SELECT * FROM refuel WHERE forVehicleId = :vehicleId")
    fun getRefuelsForVehicle(vehicleId: Long): MutableList<Refuel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg item: Refuel)

    @Delete
    suspend fun delete(vararg item: Refuel)

    @Query("DELETE FROM refuel")
    suspend fun clearTable()
}