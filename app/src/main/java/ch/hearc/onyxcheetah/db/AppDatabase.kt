package ch.hearc.onyxcheetah.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ch.hearc.onyxcheetah.converter.BigDecimalDoubleConverter
import ch.hearc.onyxcheetah.converter.LocalDateConverter
import ch.hearc.onyxcheetah.data.Refuel
import ch.hearc.onyxcheetah.data.Vehicle

@Database(entities = [Vehicle::class, Refuel::class], version = 1, exportSchema = false)
@TypeConverters(BigDecimalDoubleConverter::class, LocalDateConverter::class)
abstract class AppDatabase: RoomDatabase() {

    abstract fun vehicleDAO(): VehicleDao
    abstract fun refuelDAO(): RefuelDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            if (INSTANCE == null) {
                synchronized(this) {
                    if (INSTANCE == null) {
                        val roomInstance = Room.databaseBuilder(
                            context.applicationContext,
                            AppDatabase::class.java,
                            "OnyxCheetah"
                        ).build()
                        INSTANCE = roomInstance
                    }
                }
            }
            return INSTANCE as AppDatabase
        }
    }

}