package ch.hearc.onyxcheetah.db

import androidx.lifecycle.LiveData
import androidx.room.*
import ch.hearc.onyxcheetah.data.Vehicle

@Dao
interface VehicleDao {
    @Query("SELECT * FROM vehicle") // case nonsensitive
    fun getAll(): LiveData<MutableList<Vehicle>>

    @Insert()
    suspend fun insertAll(vararg vehicles: Vehicle) : List<Long>
    // vararg = variable (0 to n) number of arguments, separated by commas

    @Update()
    suspend fun update(vararg vehicle: Vehicle)

    @Delete
    suspend fun delete(vararg vehicle: Vehicle)

    @Query("DELETE FROM vehicle")
    suspend fun clearTable()
}