package ch.hearc.onyxcheetah.viewModel

import android.app.Application
import android.util.Log
import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import ch.hearc.onyxcheetah.data.Refuel
import ch.hearc.onyxcheetah.data.Vehicle
import ch.hearc.onyxcheetah.db.AppDatabase
import ch.hearc.onyxcheetah.enums.AutoFuelCalc
import ch.hearc.onyxcheetah.enums.FuelType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.math.BigDecimal
import java.time.LocalDate
import java.util.concurrent.Callable
import java.util.concurrent.ExecutionException
import java.util.concurrent.Executors

class VehiclesViewModel(application: Application) : AndroidViewModel(application), Observable {

    val vehicles: LiveData<MutableList<Vehicle>>// = _vehicles

    private val vehicleDao = AppDatabase.getDatabase(application).vehicleDAO()
    private val refuelDao = AppDatabase.getDatabase(application).refuelDAO()

    // TODO : TMP VAR FOR TESTING DB -> TO REMOVE
    private val ADD_TEST_DATA = false // Clear data before adding test data

    init {
        // TODO TMP CODE FOR TESTING DB -> TO REMOVE
        // FOR DEBUG, Not fully implemented as required
        // (it's async, so the deletion can be done after reading)
        // Consiere reruning the app to see the changes
        if (ADD_TEST_DATA) {
            viewModelScope.launch(Dispatchers.Main) {
                Log.i("SEEDER", "Clearing DB...");
                refuelDao.clearTable()
                vehicleDao.clearTable()

                var vehicle1: Vehicle? = null
                for (vehicle in getSampleVehicles()) {
                    if (vehicle1 == null) {
                        vehicle1 = vehicle
                    }

                    updateVehicle(vehicle).join()
                    Log.i("SEEDER", "Added vehicle: $vehicle")
                }

                for (refuel in getSampleRefuels()) {
                    updateRefuelOfVehicle(refuel, vehicle1!!).join()
                    Log.i("SEEDER", "Added refuel: $refuel for vehicle: $vehicle1")
                }
            }
        }

        vehicles = vehicleDao.getAll()
    }

    private fun getSampleVehicles(): MutableList<Vehicle> {
        val vehicle1 = Vehicle("Car 1", FuelType.SUPER98)

        val vehicle2 = Vehicle("Car 2", FuelType.DIESEL)

        return mutableListOf<Vehicle>(vehicle1, vehicle2)
    }

    private fun getSampleRefuels(): MutableList<Refuel> {
        val refuel1 = Refuel(BigDecimal("134.5"), FuelType.SUPER98)
        refuel1.forVehicleId = 1
        refuel1.date = LocalDate.of(2022, 12, 1)
        refuel1.liters = BigDecimal("25.0")
        refuel1.pricePerLiter = BigDecimal("1.9")
        refuel1.calcMissingValue(AutoFuelCalc.TOTAL_PRICE)

        val refuel2 = Refuel(BigDecimal("777.7"), FuelType.SUPER95)
        refuel2.forVehicleId = 1
        refuel2.date = LocalDate.of(2023, 1, 8)
        refuel2.liters = BigDecimal("72.8")
        refuel2.totalPrice = BigDecimal("45.5")
        refuel2.calcMissingValue(AutoFuelCalc.PRICE_PER_LITER)

        val refuel3 = Refuel(BigDecimal("777.7"), FuelType.SUPER95)
        refuel3.forVehicleId = 1
        refuel3.date = LocalDate.of(2023, 1, 9)
        refuel3.liters = BigDecimal("72.8")
        refuel3.totalPrice = BigDecimal("45.5")
        refuel3.calcMissingValue(AutoFuelCalc.PRICE_PER_LITER)

        val refuel4 = Refuel(BigDecimal("777.7"), FuelType.SUPER95)
        refuel4.forVehicleId = 1
        refuel4.date = LocalDate.of(2023, 1, 10)
        refuel4.liters = BigDecimal("72.8")
        refuel4.totalPrice = BigDecimal("45.5")
        refuel4.calcMissingValue(AutoFuelCalc.PRICE_PER_LITER)

        return mutableListOf(refuel1, refuel2, refuel3, refuel4)
    }

    fun getVehicles(): MutableList<Vehicle> {
        return vehicles.value ?: mutableListOf()
    }

    fun getRefuelsOf(vehicle: Vehicle): MutableList<Refuel> {
        // https://stackoverflow.com/questions/44364240/android-room-get-the-id-of-new-inserted-row-with-auto-generate/44364516#44364516

        var refuels = mutableListOf<Refuel>()

        val getCallable = Callable {
            refuelDao.getRefuelsForVehicle(vehicle.vehicleId)
        }
        val future = Executors.newSingleThreadExecutor().submit(getCallable)

        try {
            refuels = future.get()
        } catch (e1: InterruptedException) {
            e1.printStackTrace()
        } catch (e: ExecutionException) {
            e.printStackTrace()
        }

        return refuels
    }

    fun updateVehicle(vehicle: Vehicle) = viewModelScope.launch(Dispatchers.IO) {

        if (vehicle.vehicleId == 0L) {
            val vehicleIds = vehicleDao.insertAll(vehicle)
            vehicle.vehicleId = vehicleIds[0]
        } else {
            vehicleDao.update(vehicle)
        }
    }

    fun deleteVehicle(vehicle: Vehicle) = viewModelScope.launch(Dispatchers.IO) {
        vehicleDao.delete(vehicle)
    }

    fun updateRefuelOfVehicle(refuel: Refuel, vehicle: Vehicle) =
        viewModelScope.launch(Dispatchers.IO) {
            refuel.forVehicleId = vehicle.vehicleId
            refuelDao.insertAll(refuel)
        }

    fun deleteRefuel(refuel: Refuel) = viewModelScope.launch(Dispatchers.IO) {
        refuelDao.delete(refuel)
    }

    // === Observable for bindings ===
    // Source : https://stackoverflow.com/questions/56543945/bindable-must-be-on-a-member-in-an-observable-class
    private val callbacks: PropertyChangeRegistry by lazy { PropertyChangeRegistry() }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        callbacks.add(callback)
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        callbacks.remove(callback)
    }
}